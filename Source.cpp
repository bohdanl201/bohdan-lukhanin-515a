#include <stdio.h>
#include <stdlib.h>

int main()
{
	float a, b, x1, x2, y1, y2;
	printf("Determine does a straight line intersect ax+b=y with section with ends (x1,y1), (x2,y2)\n");
	printf(" Enter the equation of a straight line ax+b=y: \n");
	printf("    a = ");
	scanf_s("%f", &a);
	printf("    b = ");
	scanf_s("%f", &b);
	printf("  Enter the coordinates of the ends of the segment: \n");
	printf("    x1 = ");
	scanf_s("%f", &x1);
	printf("    y1 = ");
	scanf_s("%f", &y1);
	printf("    x2 = ");
	scanf_s("%f", &x2);
	printf("    y2 = ");
	scanf_s("%f", &y2);
	float f1, f2;
	f1 = a * x1 + b - y1;
	f2 = a * x2 + b - y2;
	if (f1*f2 <= 0)
		printf("Straight %fx+%f=y intersect with section with ends (%f,%f), (%f,%f)\n", a, b, x1, y1, x2, y2);
	else
		printf("Straight %fx+%f=y don't intersect with section with ends (%f,%f), (%f,%f)\n", a, b, x1, y1, x2, y2);
	system("pause");
	return 0;
}
